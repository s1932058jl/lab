up:
	docker-compose up -d
clean:
	docker-compose down --rmi all -v
migrate:
	docker-compose exec server poetry run python -m api.migrate_db