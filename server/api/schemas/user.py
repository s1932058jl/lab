from datetime import datetime
from typing import Optional

from pydantic import BaseModel, Field


class User(BaseModel):
    id: str
    name: Optional[str] = Field(None, example="佐藤佑樹")
    email: Optional[str] = Field(None, example="s1932058jl@s.chibakoudai.jp")
    emailVerified: Optional[datetime]
    image: Optional[str] = Field(None, example="https://...")

    class Config:
        orm_mode = True
